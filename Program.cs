﻿using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

namespace Programmanalyse05
{
    public class Klasse_A
    {
        protected int nr = 0;

        public Klasse_A()
            : this(1)
        {
            Console.WriteLine("A" + nr);
        }

        public Klasse_A(int pNr)
        {
            nr = pNr;
            Console.WriteLine("B" + nr);
        }

        public void Show()
        {
            Console.WriteLine($"<{nr}>");
        }
    }









    public class Klasse_B : Klasse_A
    {
        private Klasse_A parent;

        public Klasse_B()
            : this(new Klasse_A(5))
        {
            Console.WriteLine("X" + nr);
            nr = 8;
        }

        public Klasse_B(Klasse_A pParent)
        {
            parent = pParent;
            Console.WriteLine("Y" + nr);
        }

        public void NewShow()
        {
            parent.Show();
            base.Show();
        }
    }

    // Bevor der erste Konstruktor der Sub-Klasse ausgeführt wird, muss ein Konstruktor der Superklasse ausgeführt werden
    // 
    // Ohne weitere Angabe wird grundsätzlich der parameterlose Konstruktor der Superklasse ausgeführt.
    // 
    // Wenn ein parameterloser Konstruktor nicht existiert oder gezielt ein anderer Konstruktor ausgeführt werden soll,
    // muss der Konstruktor der Super-Klasse unter Verwendung des base Schlüsselwort explizit "genannt" werden.


    public class Programm
    {
        static void Main(string[] args)
        {
            Console.WriteLine("B-Objekt erstellen:");
            Klasse_B objekt1 = new Klasse_B();

            Console.WriteLine("\n\nA‐Objekt erstellen:");
            Klasse_A objekt2 = new Klasse_A(3);

            Console.WriteLine("\n\nShow‐Methode von B‐Objekt:");
            objekt1.Show();

            Console.WriteLine("\n\nNewShow‐Methode von B‐Objekt:");
            objekt1.NewShow();

            Console.WriteLine("\n\nShow‐Methode von A‐Objekt:");
            objekt2.Show();

            Console.WriteLine("\n\nA‐Objekt neu erstellen");
            objekt2 = new Klasse_A();

            Console.WriteLine("\n\nShow‐Methode von A‐Objekt:");
            objekt2.Show();

            Console.WriteLine("\n\nDer A‐Objektreferenz ein neues Objekt" +
                              " der Klasse B hinzufuegen");
            objekt2 = new Klasse_B(objekt1);

            Console.WriteLine("\n\nShow‐Methode von A‐Objekt:");
            objekt2.Show();
        }
    }
}
